# consulta-npe-service

Demo de transformación de formato JSON a trama plana y viceversa. Tecnologías utilizadas:
    - IBM Integration Toolkit.
    - WebSphere MQ Explorer.

# *** ENUNCIADO ***
* Crear las siguientes colas en tu gestor de colas local [WebSphere MQ Explorer]:

- API.TO.BABROKER.REQ.CL
- BABROKER.TO.AS400.REQ.CL
- AS400.TO.BABROKER.RSP.CL
- BABROKER.TO.API.RSP.CL

* Campos de entrada de la trama en formato JSON
			
|   TAG NAME    |   DESCRIPCION |   TAMAÑO  |	TIPO DATO   |	FORMATO	VALOR   |   COMENTARIOS |
|   ------  |   ------  |   ------  |   ------  |   ------  |   ------  |
|   tipoCodigo  |	Tipo de Codigo	|   2   |	CHAR    |       |       |			
|   codigoNpe   |	NPE o Código de Barras  |	82  |	CHAR    |       |       |			
|   fecha   |	Fecha   |	6   |	CHAR    |       |       |			
|   hora    |	Hora    |	6   |	CHAR    |       |       |			

* Campos de salida de la trama formato trama plana

|   TAG NAME    |	DESCRIPCION |	TAMAÑO  |	TIPO DATO   |	FORMATO	VALOR   |	COMENTARIOS |
|   ------  |   ------  |   ------  |   ------  |   ------  |   ------  |
|   codigoTransaccion   |	Código de Transacción   |	4   |	CHAR    |       |       |			
|   montoPagar  |	Monto a Pagar   |	13  |	NUM |       |       |
|   montoMora   |	Monto Mora  |	13  |	NUM |       |       |			
|   montoTotal  |	Monto Total |	13  |	NUM |       |       |
|   tipoPago    |	Tipo de Pago    |	1   |	CHAR    |       |       |
|   montoMinimo |	Monto Mínimo    |	13  |	NUM |       |       |
|   nombreCompania  |	Nombre de la Compañía   |	35  |	CHAR    |       |       |
|   identificador   |	Identificador   |	13  |	CHAR    |       |       |
|   codigoColector  |	Código del Colector |	10  |	CHAR    |       |       |
|   inficaforLinea  |	Identificador de Línea  |	1   |	CHAR    |       |       |
|   nombreCliente   |	Nombre del Cliente  |	35  |	CHAR    |       |       |
|   saldoEnergia    |	Saldo Energía Eléctrica |	12  |	NUM |       |       |
|   saldoAlcaldia   |	Salgo Alcaldía  |	12  |	NUM |       |       |
|   saldoReconexion |	Saldo Reconexión    |	12  |	NUM |       |       |

* El siguiente Request será puesto en la cola “API.TO.BABROKER.REQ.CL” y el mensaje de salida caerá en la cola: “BABROKER.TO.AS400.REQ.CL” en forma de trama plana.
**JSON- Request**

{
	"metadata": {
		"messageType": "Request",
		"messageId": "000000000000000",
		"applicationId": "9999999",
		"serviceId": "9999999",
		"datetime": "20210212140310"
	},
	"data": {
		"oTipoCodigo": "NP",
		"oCodigoNPE": "2307000000061501043831205192                                                      ",
		"oFecha": "190516",
		"oHora": "094606"
	}
}

**Nota:** la Trama Plana de Salida será igual a esta: 
“999999900000000999999900000000000000                                                                                                                                                                                                                                                                                                                                                                            NP2307000000061501043831205192                                                      190516094606”



* Luego se depositará el siguiente mensaje en la cola “AS400.TO.BABROKER.RSP.CL”:

“999999900000000000000020211021093301000000Proceso ejecutado satisfactoriamente                                                                03100000000003.250000000001.000000000004.25 0000000000.00ANDA                               00000000026590037802000 JULIO CESAR                        000000000.00000000000.00000000000.00”

 y deberá devolver un Response como el del Ejemplo:


**JSON-Response**

{
	"metadata": {
		"messageType": "Response",
		"messageId": "000000000000000",
		"code": "000000",
		"description": "Proceso ejecutado satisfactoriamente                                                                ",
		"applicationId": "9999999",
		"serviceId": "9999999",
		"datetime": "20211021093301"
	},
	"data": {
		"codigoTransaccion": "0310",
		"montoPagar": "0000000003.25",
		"montoMora": "0000000001.00",
		"montoTotal": "0000000004.25",
		"tipoPago": " ",
		"montoMinimo": "0000000000.00",
		"nombreCompania": "ANDA                               ",
		"identificador": "0000000002659",
		"codigoColector": "0037802000",
		"identificadorLinea": " ",
		"nombreCliente": "JULIO CESAR                        ",
		"saldoEnergia": "000000000.00",
		"saldoAlcaldia": "000000000.00",
		"saldoReconexion": "000000000.00"
	}
}
